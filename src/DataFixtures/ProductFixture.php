<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

/**
 * Class ProductFixture
 * @package App\DataFixtures
 */
class ProductFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 1000; $i++) {
            $product = (new Product())
                ->setName($faker->words(2, true))
                ->setPrice($faker->randomFloat(2,15,10000))
                ->setDescription($faker->sentences(5, true))

            ;

            for ($j = 0; $j < $faker->numberBetween(1,3); $j++) {
                $product
                    ->addCategories(
                        $this->getReference('Category_' . $faker->numberBetween(1,8))
                    )
                ;
            }

            $product->setImage("logo_@2x.png");

            $this->setReference('Product_' .$i, $product);
            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            CategoryFixture::class,
        );
    }
}
