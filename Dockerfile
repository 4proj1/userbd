FROM bitnami/symfony

COPY  . /app/project/

WORKDIR /app/project

CMD ["./bin/startup.sh"]

RUN composer update

COPY  . /app/project/