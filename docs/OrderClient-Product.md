# OrderClient_Product

This table links a product with an order.


## Columns

### product_id

Type : int(11)  
Definition : Identifier of a product.  
Example of values : 12345

### order_id

Type : int(11)  
Definition : Identifier of an order.  
Example of values : 12345


## Constraints

### FOREIGN KEY(product_id, Product)
Cf. [Product documentation](Product.md)

### FOREIGN KEY(order_id, OrderClient)
Cf. [OrderClient documentation](OrderClient.md)