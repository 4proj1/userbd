# Product

This table represent a product.


## Columns

### id

Type : int(11)  
Definition : Identifier of a product.  
Example of values : 12345

### name

Type : varchar2(255)  
Definition : Name of the product.  
Example of values : Pie

### price

Type : double  
Definition : Price of the product.  
Example of values : 123.45

### image

Type : varchar2(255)  
Definition : Name of the image of this product.  
Example of values : Pie.jpg

### description

Type : varchar2(255)  
Definition : Description of this product.  
Example of values : This is a pie.

### provider_id

Type : int(11)  
Definition : Id of the provider that provide this product.  
Example of values : 12345



## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(provider_id, Provider)
Cf. [Provider documentation](Provider.md)