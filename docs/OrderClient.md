# OrderClient

This table represent an order made by a client.


## Columns

### id

Type : int(11)  
Definition : Identifier of a product.  
Example of values : 12345

### order_date

Type : date  
Definition : Date of the order.  
Example of values : 01/01/1995

### client_id

Type : int(11)  
Definition : Id of the user that made this order.  
Example of values : 12345


## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(client_id, User)
Cf. [User documentation](User.md)