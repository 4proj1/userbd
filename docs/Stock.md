# Stock

This table represent a number of products that are available.


## Columns

### id

Type : int(11)  
Definition : Identifier of a stock.  
Example of values : 12345

### quantity

Type : int(11)  
Definition : Quantity of products left.  
Example of values : 12

### product_id

Type : int(11)  
Definition : Id of the product that is concerned by this count.  
Example of values : 12345



## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(product_id, Product)
Cf. [Product documentation](Product.md)