# Provider

This table represent a provider.


## Columns

### id

Type : int(11)  
Definition : Identifier of a provider.  
Example of values : 12345

### SIRET

Type : varchar2(14)  
Definition : SIRET of the provider.  
Example of values : 01234567891234

### name

Type : varchar2(255)  
Definition : Name of the provider.  
Example of values : Peas&Pie vendor'n co

### address

Type : varchar2(255)  
Definition : Address of the provider.  
Example of values : 123 street of the street

### is_blocked

Type : boolean  
Definition : Says if this provider is blocked.  
Example of values : false

### contact_id

Type : int(11)  
Definition : Id of the user that represent this provider.  
Example of values : 12345



## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(contact_id, User)
Cf. [User documentation](User.md)