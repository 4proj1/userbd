# Category

This table represent a category.


## Columns

### id

Type : int(11)  
Definition : Identifier of a category.  
Example of values : 12345

### name

Type : varchar2(255)  
Definition : Name of the category.  
Example of values : Vegetables


## Constraints

### PRIMARY KEY(id)
