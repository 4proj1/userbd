# User

This table represent an user's account.


## Columns

### id

Type : int(11)  
Definition : Identifier of a user.  
Example of values : 12345

### last_name

Type : varchar2(255)  
Definition : Last name of the user.  
Example of values : Dupont

### first_name

Type : varchar2(255)  
Definition : First name of the user.  
Example of values : Michel

### birthday

Type : date  
Definition : Birthday of the user.  
Example of values : 01/01/1995

### register_at

Type : datetime  
Definition : Time of the creation of the user in the database.  
Example of values : 1532054815

### username

Type : varchar2(255)  
Definition : Username of the user.  
Example of values : TheDarkKevinOfThe93

### sex

Type : varchar2(255)  
Definition : Sex of the user.  
Example of values : f

### email

Type : varchar2(255)  
Definition : Email of the user.  
Example of values : dark.kevin@yahoo.fr

### phone

Type : varchar2(255)  
Definition : Phone number of the user.  
Example of values : 0213456789

### address

Type : varchar2(255)  
Definition : Address of the user.  
Example of values : 123 street of the street

### diet

Type : longtext  
Definition : Json list of the user's diet.  
Example of values : 12345

### allergies

Type : longtext  
Definition : Json list of the user's allergies.  
Example of values : 12345

### password

Type : varchar2(255)  
Definition : Encrypted password of the user.  
Example of values : DEADBEEF165413

### is_blocked

Type : boolean  
Definition : Flag to say if this account is blocked or not.  
Example of values : false

### roles

Type : longtext  
Definition : Json list of the user's roles.  
Example of values : 12345



## Constraints

### PRIMARY KEY(id)