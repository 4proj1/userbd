# Product_Category

This table links a product with a category.


## Columns

### product_id

Type : int(11)  
Definition : Identifier of a product.  
Example of values : 12345

### category_id

Type : int(11)  
Definition : Identifier of a category.  
Example of values : 12345


## Constraints

### FOREIGN KEY(product_id, Product)
Cf. [Product documentation](Product.md)

### FOREIGN KEY(category_id, Category)
Cf. [Category documentation](Category.md)